import jenkins.model.Jenkins

gitJobs = Jenkins.instance.items.findAll { job ->
  job instanceof hudson.model.FreeStyleProject && job.builders.any { builder ->
    (builder instanceof hudson.tasks.Ant) && !builder.properties.contains('dir.svn.test=./target/test-classes')
  }
}

gitJobs.each { println it.name }
null
