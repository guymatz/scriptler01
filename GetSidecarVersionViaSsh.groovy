//sidecarApp = 'web'
//sidecarAppServer = 'nyc-scy-q03'

def getVer() {

  def ver
  try {
    def sout = new StringBuffer(), serr = new StringBuffer()
    def cmd = "ssh ansible@${sidecarAppServer}  -o StrictHostKeyChecking=no -o ConnectTimeout=3  rpm -qa sidecar-${sidecarApp}"
	ver = cmd.execute().text
    ver = ver.split('-')[2]
  }
  catch (Exception e) {
    return ["Uh oh", "ver = ${ver}:", "${e}"]
  }

  if (ver ==~ /[0-9]+/) {
    return [ver]
  }
  else {
    return ['Uh Oh', "OH NOOOOO"]
  }
}

return getVer()