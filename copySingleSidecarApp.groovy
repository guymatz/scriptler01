env = build.getEnvironment()
/*
for (e in env) {
  println e
}
*/
println "sidecarApp: ${sidecarApp}"
println "MASTER_BUILD_NUMBER: ${MASTER_BUILD_NUMBER}"
println "fromRepo: ${fromRepo}"
println "toRepo: ${toRepo}"

//  These two env vars are gotten via Jenkins credentials
artifactoryUser = env.ARTIFACTORY_USERNAME
artifactoryPassword = env.ARTIFACTORY_PASSWORD
// println "u/p: ${artifactoryUser}:${artifactoryPassword}"

afactServer = 'nyc-artfc-d01'
copyBaseUrl = "http://${afactServer}/artifactory/api/copy/${fromRepo}"
checkBaseUrl = "http://${afactServer}/artifactory/api/storage/${toRepo}"

def promote(app, build_number, baseUrl) {
  
  filePath = '/RPMS/noarch/sidecar-' + app +
                  "-${build_number}-1.el6.noarch.rpm"
  checkUrl = checkBaseUrl + filePath
  try {
    println "Checking ${checkUrl}"
    t = checkUrl.toURL().text
    //println "t = ${t}"
    if (t.contains(filePath)) {
      println "${app}-${build_number} already exists in ${toRepo}"
      return
    }
  }
  catch (FileNotFoundException e) {
    println "Promoting ${app}-${build_number} . . ."
  }
  catch (Exception e) {
    println "Something went wrong: " + e
    return
  }
  
  
  copyFromUrl = copyBaseUrl + filePath
  copyUrl = copyFromUrl + "?to=/${toRepo}/RPMS/noarch/"
  println "Hitting the URL: ${copyUrl}"
  url = new URL(copyUrl)
  connection = url.openConnection()
  connection.setRequestMethod("POST")
  
  basicAuth = "${artifactoryUser}:${artifactoryPassword}".getBytes().encodeBase64().toString()
  connection.setRequestProperty("Authorization", "Basic ${basicAuth}")
  connection.setRequestProperty("Content-Type", "application/json")
  connection.setRequestProperty("Accept", "application/json")
  connection.doOutput = true

  writer = new OutputStreamWriter(connection.outputStream)
  writer.flush()
  writer.close()
  try {
  	connection.connect()
  	println connection.responseCode
  	headerFields = connection.getHeaderFields()
  	headerFields.each {println it}
    if (connection.responseCode > 299) {
      return false
    }
    else {
      return true
    }
  }
  catch(e) {
    println "Failure!!: ${e}"
    return false
  }
}

// MAIN
println "Promoting sidecar-${sidecarApp} version: ${MASTER_BUILD_NUMBER}"
// copy RPMs to next repo
return promote(sidecarApp, MASTER_BUILD_NUMBER, copyBaseUrl)
