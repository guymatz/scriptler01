env = build.getEnvironment()
/*
for (e in env) {
  println e
}
*/
println "sidecarApp: ${sidecarApp}"
println "BUILD_NUMBER: ${BUILD_NUMBER}"
println "fromRepo: ${fromRepo}"
println "toRepo: ${toRepo}"

//  These two env vars are gotten via Jenkins credentials
artifactoryUser = env.ARTIFACTORY_USERNAME
artifactoryPassword = env.ARTIFACTORY_PASSWORD

afactServer = 'nyc-artfc-d01'
copyBaseUrl = "http://${afactServer}/artifactory/api/copy/${fromRepo}"

def promote(app, build_number, baseUrl) {
  copyFromUrl = baseUrl + '/RPMS/noarch/sidecar-' + app +
                  "-${build_number}-1.el6.noarch.rpm"
  copyUrl = copyFromUrl + "?to=/${toRepo}/RPMS/noarch/"
  println "Hitting the URL: ${copyUrl}"
  url = new URL(copyUrl)
  connection = url.openConnection()
  connection.setRequestMethod("POST")
  
  basicAuth = "${artifactoryUser}:${artifactoryPassword}".getBytes().encodeBase64().toString()
  connection.setRequestProperty("Authorization", "Basic ${basicAuth}")
  connection.setRequestProperty("Content-Type", "application/json")
  connection.setRequestProperty("Accept", "application/json")
  connection.doOutput = true

  writer = new OutputStreamWriter(connection.outputStream)
  writer.flush()
  writer.close()
  try {
  	connection.connect()
  	println connection.responseCode
  	headerFields = connection.getHeaderFields()
  	headerFields.each {println it}
  }
  catch(e) {
    println "Failure!!: ${e}"
    return
  }
}

// MAIN
//println "Using Artifactory creds, part 1: ${artifactoryUser}:${artifactoryPassword}"
println "Promoting sidecar-${sidecarApp} version: ${BUILD_NUMBER}"
//  Loop and copy RPMs to next repo
    promote(sidecarApp, BUILD_NUMBER, copyBaseUrl)
