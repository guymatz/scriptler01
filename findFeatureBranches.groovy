proc = "git ls-remote --heads ${REPO}".execute()
proc.waitFor()
println proc.err.text
featureBranchNames = proc.in.text.split('[\n]')*.replaceFirst('.*refs/heads/(.*)', '$1').findAll {
  it.startsWith("feature/")
}*.replaceFirst('feature/', '')
for (b in featureBranchNames) {
  println b
}