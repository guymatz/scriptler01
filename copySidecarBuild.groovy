sidecar_version = [
  'data': sidecarDataVersion,
  'web': sidecarWebVersion,
  'render': sidecarRenderVersion
  ]

env = build.getEnvironment()
//  These two env vars are gotten via Jenkins credentials
artifactoryUser = env.ARTIFACTORY_USERNAME
artifactoryPassword = env.ARTIFACTORY_PASSWORD

/*
println "ENV:"
build.getEnvironment().sort({a,b -> a.getKey().toLowerCase() <=> b.getKey().toLowerCase() } ).each { k,v ->
  println "${k} = ${v}"
}
*/
afactServer = 'nyc-artfc-d01'
copyBaseUrl = "http://${afactServer}/artifactory/api/copy/${fromRepo}"

def promote(app) {
  copyFromUrl = copyBaseUrl + '/RPMS/noarch/sidecar-' + app +
                  "-${sidecar_version[app]}-1.el6.noarch.rpm"
  copyUrl = copyFromUrl + "?to=/${toRepo}/RPMS/noarch/"
  println "Hitting the URL: ${copyUrl}"
  url = new URL(copyUrl)
  connection = url.openConnection()
  connection.setRequestMethod("POST")
  
  basicAuth = "${artifactoryUser}:${artifactoryPassword}".getBytes().encodeBase64().toString()
  connection.setRequestProperty("Authorization", "Basic ${basicAuth}")
  connection.setRequestProperty("Content-Type", "application/json")
  connection.setRequestProperty("Accept", "application/json")
  connection.doOutput = true

  writer = new OutputStreamWriter(connection.outputStream)
  writer.flush()
  writer.close()
  try {
  	connection.connect()
    println "Response Code: ${connection.responseCode}"
    println "response Text:\n${connection.responseMessage}\n"
  }
  catch(e) {
    println "Failure!!: ${e}"
    return
  }
}

// MAIN
//println "Using Artifactory creds, part 1: ${artifactoryUser}:${artifactoryPassword}"
println "Promoting sidecar-web version: ${sidecarWebVersion}"
println "Promoting sidecar-data version: ${sidecarDataVersion}"
println "Promoting sidecar-render version: ${sidecarRenderVersion}"
println "And here's why: ${sidecarPromotionComment}"
println "And by: ${sidecarPromoter}"
//  Loop and copy RPMs to next repo
sideCarApps = ['data', 'web','render']
sideCarApps.each { app ->
  promote(app)
}
