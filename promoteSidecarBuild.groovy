import groovy.json.JsonSlurper

sidecar_version = [
  'data': sidecarDataVersion,
  'web': sidecarWebVersion,
  'render': sidecarRenderVersion
  ]

//  These two env vars are gotten via Jenkins credentials
def env = build.getEnvironment()
//  These two env vars are gotten via Jenkins credentials
def artifactoryUser = env.ARTIFACTORY_USERNAME
def artifactoryPassword = env.ARTIFACTORY_PASSWORD

def qa_repo = 'sidecar-qa'
def preprod_repo = 'sidecar-preprod'
def copy_artifact = 'true'
def afactServer = 'nyc-artfc-d01'
def promotionBaseUrl = "http://${afactServer}/artifactory/api/build/promote"

def sideCarApps = ['data', 'web','render']
sideCarApps.each { app ->
  promote(app)
}

def promote(app) {
  post_data = [
    "comment": sidecarPromotionComment,
    "ciUser": sidecarPromoter,
    "dryRun": false,
    "targetRepo": preprod_repo,
    "copy": copy_artifact,
    ]
  promotionUrl = promotionBaseUrl + '/sidecar-' + app + '-master/' + sidecar_version[app]
  println "Hitting the URL: ${promotionUrl}"
  query = '{"status":"tests passed","targetRepo":"sidecar-preprod"}'
  url = new URL(promotionUrl)
  connection = url.openConnection()
  connection.setRequestMethod("POST")
  
  //println "Using Artifactory creds, part 2: ${artifactoryUser}:${artifactoryPassword}"
  basicAuth = "${artifactoryUser}:${artifactoryPassword}".getBytes().encodeBase64().toString()

  connection.setRequestProperty("Authorization", "Basic ${basicAuth}")
  connection.setRequestProperty("Content-Type", "application/json")
  connection.setRequestProperty("Accept", "application/json")

  connection.doOutput = true

  writer = new OutputStreamWriter(connection.outputStream)
  writer.write(query)
  writer.flush()
  writer.close()
  connection.connect()
  println connection.responseCode
  headerFields = connection.getHeaderFields()
  headerFields.each {println it}
}
