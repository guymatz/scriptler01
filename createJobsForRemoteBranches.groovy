import jenkins.model.*
  
uriToBranchNames = [:]

def branchesNamesAt(scm) {
  scm.repositories.collect { repo ->
    repo.uris.collect { uri ->
	  branchNames = uriToBranchNames[uri]
      
      if (!branchNames) {
        // println "getting remote branches at ${uri}"
        proc = "git ls-remote --heads ${uri}".execute()
        proc.waitFor()
        // Strip off SHA1 and refs/heads prefix, leaving leading forward slash (/)
        branchNames = proc.in.text.split('[\n]')*.replaceAll('.*refs/heads(.*)', '$1')
    	uriToBranchNames[uri] = branchNames
      }
      
      branchNames
    }
  }.flatten() as Set
}

// Find all jobs using Git
gitJobs = Jenkins.instance.getView('ecommerce').items.findAll { job ->
  //println "Checking scm ${job}: ${job.scm?.type}"
  job.scm?.type.endsWith("GitSCM")                                                                                   
}

// Find all Git jobs with branches
gitJobsWithRemoteBranches = gitJobs.findAll{ job ->
  //println "Checking job ${job}"
  branchNameRegexes = job.scm.branches.collect { branch -> toBranchNameRegex(branch) }
  branchNameRegexes.every { branchNameRegex ->
    branchesNamesAt(job.scm).any { branchName ->
      branchName.matches(branchNameRegex)
    }
  }
}

def toBranchNameRegex(branch) {
  // Remove 'remotes/' and 'origin/' prefixes, if specified
  regex = branch.name.replace('remotes/', '').replace('origin/', '').replace('refs/heads/', '') 
  
  // After stripping prefixes above, if name pattern does not start with a
  // wildcard (*), add a leading forward slash (/) to match the leading forward
  // slash of the remote branch name (as produced by branchNamesAt)
  regex = regex.replaceAll(/^([^*].*)/, '/$1')
  
  // Convert wildcard (*) to regex that matches everything except forward
  // slashes ([^/]*)
  regex = regex.replaceAll(/[*]/, '[^/]*')
  
  // In case the previous replacement replaced 2 consecutive wildcards (**),
  // replace with regex that matches everything -- including forward slashes
  // -- (.*)
  regex = regex.replaceAll('(\\Q[^/]*\\E){2}', '.*')
}


def jenkins = Jenkins.instance
features_view = jenkins.getView('features')
for (j in gitJobsWithRemoteBranches) {
  for (b in j.scm.branches) {
    if (b.toString().contains('feature')) {
      // def new_job = j.name + '-' + b.toString().split('/')[-1]
      def new_job_name = j.name + "-" + b.toString().split('/')[-1]
      println "Copying job for " + j.name + " to " + new_job_name
      def job_template = jenkins.getItem(j.name)
      // println job_template
      def new_job
      try {
      	new_job = jenkins.copy(job_template,new_job_name)
        new_job.disable()
      }
      catch (Exception e) {
        println "There was a problem copying ${job_template} to ${new_job_name}: " + e
      }
      try {
        features_view.add(new_job)
      }
      catch (Exception e) {
        println "There was a problem adding ${new_job_name} to the 'features' view: " + e
      }
    }
  }
}
jenkins.reload()
