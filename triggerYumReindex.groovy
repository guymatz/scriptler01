log("Getting Environnent . . .")
env = build.getEnvironment()
//  These two env vars are gotten via Jenkins credentials
log("Getting Artifactory User/Password . . .")
artifactoryUser = env.ARTIFACTORY_USERNAME
artifactoryPassword = env.ARTIFACTORY_PASSWORD
//log("u/p: ${artifactoryUser}:${artifactoryPassword}")

afactServer = 'nyc-artfc-d01'
reindexBaseUrl = "http://${afactServer}/artifactory/api/yum/sidecar-"

def reclacimate(env) {
  // See https://www.jfrog.com/confluence/display/RTF/Artifactory+REST+API#ArtifactoryRESTAPI-CalculateYUMRepositoryMetadata
  // reindexUrl = reindexBaseUrl + env + '?async=1'
  reindexUrl = reindexBaseUrl + env
  log("Hitting the URL: ${reindexUrl}")
  url = new URL(reindexUrl)
  connection = url.openConnection()
  connection.setRequestMethod("POST")
  
  // println "Setting up auth with artifactoryUser = ${artifactoryUser}/${artifactoryPassword} . . ."
  basicAuth = "${artifactoryUser}:${artifactoryPassword}".getBytes().encodeBase64().toString()
  connection.setRequestProperty("Authorization", "Basic ${basicAuth}")
  // connection.setRequestProperty("Content-Type", "application/json")
  // connection.setRequestProperty("Accept", "application/json")
  // connection.doOutput = true

  /*
  writer = new OutputStreamWriter(connection.outputStream)
  writer.flush()
  writer.close()
  */
  try {
    log("Re-indexing!  This could take a couple of minutes . . .")
  	connection.connect()
    if (connection.responseCode >= 400) {
      log("Response Code: ${connection.responseCode}")
      log("Response Message: ${connection.responseMessage}")
      return false
    }
    else {
      log("Reindex appears to have completed")
      return true
    }
  }
  catch(e) {
    log("Failure!!: ${e}")
    return false
  }
}

def log(String msg) {
  println new Date().format("yyyy.MM.dd G 'at' HH:mm:ss") + " - " + msg
}

// MAIN
log("About to run reclacimate . . .")
return reclacimate(SIDECAR_ENV)