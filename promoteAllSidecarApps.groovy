// prepare variables for subscript and evaluate it:
String subscript = "promoteSingleSidecarApp.groovy";
println "executing subscript ${subscript}"
// variables to be passed to the subscript must not be bound to the current scope, so no def or String here
status = (action == "start" ? "started" : "stopped")
  
def s = ScriptlerConfiguration.getConfiguration().getScriptById(subscript)
File scriptSrc = new File(ScriptlerManagment.getScriptDirectory(), s.getScriptPath());