import hudson.model.*


disableChildren(Hudson.instance.items.sort())

def disableChildren(items) {
  for (item in items) {
    println "Checking ${item.name}"
    //println "Name: ${item.class.canonicalName}"
    if (item.name == 'sidecar-web-master' || item.name =~ /sidecar-ci-.*-smoke-tests/ || item.name =~ /.*Tagging.*/) {
      println "	ENABLING ${item.name}"
      item.disabled=false
      item.save()
    } else if (item.class.canonicalName != 'com.cloudbees.hudson.plugins.folder.Folder') {
      println "	Disabling ${item.name}"
	  item.disabled=true
	  item.save()
	  //println(item.name)
    } else {
      println "Not sure what to do with ${item}"
 	  //disableChildren(((com.cloudbees.hudson.plugins.folder.Folder) item).getItems())
    }
  }
}