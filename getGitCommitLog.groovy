import jenkins.model.*
import groovy.json.JsonSlurper

def getGitCommitLog(String app, String old_build, new_build) {
  //println "Getting git Log Entries"
  jenkinsJob = "sidecar-${app}-master"
  job = Jenkins.instance.getJob(jenkinsJob)

  older_build = job.getBuildByNumber(olderBuild.toInteger())
  newer_build = job.getBuildByNumber(newerBuild.toInteger())
  workdir = job.getWorkspace().toString()
  
  older_build_sha =  older_build.getAction(hudson.plugins.git.util.BuildData.class).lastBuiltRevision.sha1String
  newer_build_sha =  newer_build.getAction(hudson.plugins.git.util.BuildData.class).lastBuiltRevision.sha1String
  
  // println "Workdir: " + workdir
  
  cmd = ['/bin/bash', '-c', "git shortlog --no-merges ${older_build_sha}..${newer_build_sha}"]
  println "cmd: " + cmd.join(" ")
  proc = cmd.execute(null, new File(workdir))
  proc.waitForOrKill(1000)
  //println "error: " + proc.errorStream.text
  // print proc.text
  return proc.text
}

def getAppVersionViaSSH(app, sidecar_server='nyc-scy-q03') {
  println "Getting ${app} version via SSH"
  remote_user='ansible'
  ver = ''
  try {
    sout = new StringBuffer()
    serr = new StringBuffer()
    cmd = "ssh ${remote_user}@${sidecar_server}  -o StrictHostKeyChecking=no \
                -o ConnectTimeout=3  /bin/rpm -qa sidecar-${app}"
    // println "cmd = " + cmd
    proc = cmd.execute()
    proc.consumeProcessOutput(sout, serr)
    proc.waitForOrKill(5000)
    // println "Trying to get via ssh: ${sout} (${serr})"
    versions = sout.toString().split()
    // println "ver = " + versions
    ver = versions[0].split("-")[2]
    // println "Got ${app} version ${ver} via SSH"
  }
  catch (Exception e) {
    println "Error getting version - ${ver} of ${app} from ${sidecar_server}: ${e}"
  }

  if (ver ==~ /[0-9]+/) {
    println "Got ${app} version ${ver} via SSH"
    return ver
  }
  else {
    println "Problem getting version - ${ver} - of ${app} from ${sidecar_server}"
    return ver
  }
}


def getAppVersionFromArtifactory(app, repo='qa') {
  println "Getting ${app} version from Artifactory"
  aUser = 'admin'
  aPassword = 'password'
  urlString = "http://nyc-artfc-d01/artifactory/api/search/aql"
  queryString = """items.find(
      {
          \"repo\": {\"\$eq\": \"sidecar-qa\"},
          \"name\": {\"\$match\":\"sidecar-${app}*\"}
      }
  ).include(\"name\", \"created\"
  ).sort({\"\$desc\" : [\"created\"]}
  ).limit(1)
  """
  
/*
  queryString = '''items.find(
    {
        "repo": {"$eq": "sidecar-qa"},
        "name": {"$match":"sidecar-web*"}
    }
    ).include("name", "created"
    ).sort({"$asc" : ["created"]}
    ).limit(1)
  '''
*/
  // print "qString = " + queryString
  basicAuth = "${aUser}:${aPassword}".getBytes().encodeBase64().toString()
  url = new URL(urlString)
  connection = url.openConnection()
  connection.setRequestMethod("POST")
  connection.setRequestProperty("Content-Type", "application/json")
  connection.setRequestProperty("Accept", "application/json")
  connection.setRequestProperty("Authorization", "Basic ${basicAuth}")
  connection.doOutput = true
  
  writer = new OutputStreamWriter(connection.outputStream)
  writer.write(queryString)
  writer.flush()
  writer.close()
  connection.connect()
  
  json = new JsonSlurper().parseText(connection.content.text)
  rpm = json['results'][0]['name']
  ver = rpm.split('-')[2]
  println "Got ${app} version ${ver} from artifactory"
  return ver
}

// MAIN
apps = ['data', 'render', 'web']
changes = ''
for (app in apps) {
  println app.toUpperCase()
  olderBuild = getAppVersionViaSSH(app)
  newerBuild = getAppVersionFromArtifactory(app)
  changes = changes + getGitCommitLog(app, olderBuild, newerBuild)
}

println changes