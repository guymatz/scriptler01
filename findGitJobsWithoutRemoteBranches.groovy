import jenkins.model.Jenkins

uriToBranchNames = [:]

def branchesNamesAt(scm) {
    scm.repositories.collect { repo ->
        repo.uris.collect { uri ->
            branchNames = uriToBranchNames[uri]
      
            if (!branchNames) {
                println "getting remote branches at ${uri}"
                proc = "git ls-remote --heads ${uri}".execute()
                proc.waitFor()
                // Strip off SHA1 and refs/heads prefix, leaving leading forward slash (/)
                branchNames = proc.in.text.split('[\n]')*.replaceAll('.*refs/heads(.*)', '$1')
                uriToBranchNames[uri] = branchNames
            }
      
            branchNames
        }
    }.flatten() as Set
}

def toBranchNameRegex(branch) {
    // Remove unwanted prefixes, if specified
    regex = branch.name.replace('remotes/', '').replace('origin/', '').replace('refs/heads/', '')
  
    // After stripping prefixes above, if name pattern does not start with a
    // wildcard (*), add a leading forward slash (/) to match the leading forward
    // slash of the remote branch name (as produced by branchNamesAt)
    regex = regex.replaceAll(/^([^*].*)/, '/$1')
  
    // Convert wildcard (*) to regex that matches everything except forward
    // slashes ([^/]*)
    regex = regex.replaceAll(/[*]/, '[^/]*')
  
    // In case the previous replacement replaced 2 consecutive wildcards (**),
    // replace with regex that matches everything -- including forward slashes
    // -- (.*)
    regex = regex.replaceAll('(\\Q[^/]*\\E){2}', '.*')
}

// Find all jobs using Git
gitJobs = Jenkins.instance.getView('features').items.findAll { job ->
    job.scm?.type.endsWith("GitSCM")
}

// Find all Git jobs where every referenced branch does not exist remotely
gitJobsWithoutRemoteBranches = gitJobs.findAll { job ->
    branchNameRegexes = job.scm.branches.collect { branch -> toBranchNameRegex(branch) }
    branchNameRegexes.every { branchNameRegex ->
        !branchesNamesAt(job.scm).any { branchName ->
            branchName.matches(branchNameRegex)
        }
    }
}

println ""

if (gitJobsWithoutRemoteBranches.empty) {
    println "All jobs refer to existing remote branches"
} else {
    maxJobNameSize = gitJobsWithoutRemoteBranches*.name*.size().max()
  
    println String.format("%-${maxJobNameSize}s  %s", "Job Name", "Missing Branch Name")
    println "${'-' * maxJobNameSize}  ${'-' * 50}"
  
    gitJobsWithoutRemoteBranches.each { job ->
        println String.format("%-${maxJobNameSize}s  %s", job.name, job.scm.branches*.name)
    }
}

null
